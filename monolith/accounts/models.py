from django.contrib.auth.models import AbstractUser
from django.db import models


class User(AbstractUser):
    email = models.EmailField(unique=True)


class Accounts(models.Model):
    email = models.EmailField(null=False)
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    is_active = models.BooleanField(null=False)
    updated = models.DateTimeField(auto_now=True)

    def save(self, *args, **kwargs):
        if self.updated is not None:
            self.update = self.updated.isoformat()
        super(Accounts, self).save(*args, *kwargs)
