import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    # Create a dictionary for the headers to use in the request
    query_str = f"{city}+{state}"
    url = f"https://api.pexels.com/v1/search?query={query_str}&per_page=1"
    headers = {"Authorization": PEXELS_API_KEY}
    r = requests.get(url, headers=headers)
    r = json.loads(r.content)
    print(r)
    picture_url = r["photos"][0]["src"]["original"]
    try:
        return {"picture_url": picture_url}
    except:
        return {"picture_url": None}

    # Create the URL for the request with the city and state
    # Make the request
    # Parse the JSON response

    # Return a dictionary that contains a `picture_url` key and
    #   one of the URLs for one of the pictures in the response


def get_weather_data(city, state):
    print(city)
    print(state)
    # Create the URL for the geocoding API with the city and state
    url = "http://api.openweathermap.org/geo/1.0/direct"
    params = {
        "q": f"{city},{state},US",
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY,
    }
    r = requests.get(url, params=params)
    r = json.loads(r.content)
    print(r)
    latitude = r[0]["lat"]
    longitude = r[0]["lon"]
    # Make the request
    # Parse the JSON response
    # Get the latitude and longitude from the response
    url = "https://api.openweathermap.org/data/2.5/weather"
    params = {
        "lat": latitude,
        "lon": longitude,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    weather = requests.get(url, params=params)
    weather = json.loads(weather.content)
    temperature = weather["main"]["temp"]
    description = weather["weather"][0]["description"]
    try:
        return {"temperature": temperature, "description": description}
    except:
        return None
    # Create the URL for the current weather API with the latitude
    #   and longitude
    # Make the request
    # Parse the JSON response
    # Get the main temperature and the weather's description and put
    #   them in a dictionary
    # Return the dictionary
