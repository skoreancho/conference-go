from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Attendee, ConferenceVO, AccountVO
from django.views.decorators.http import require_http_methods
import json


class ConferenceVODetailEncoder(ModelEncoder):
    model = ConferenceVO
    properties = [
        "name",
        "import_href",
    ]


class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "name",
    ]


class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "email",
        "name",
        "company_name",
        "created",
        "conference",
    ]

    encoders = {
        "conference": ConferenceVODetailEncoder(),
    }

    def get_extra_data(self, o):
        # Get the count of AccountVO objects with email equal to o.email
        count = AccountVO.objects.filter(email=o.email).count()
        # test = AccountVO.objects.get(email=o.email)
        # print(test)
        print(count)
        print(o.email)
        # Return a dictionary with "has_account": True if count > 0
        if count > 0:
            return {"has_account": True}
        # Otherwise, return a dictionary with "has_account": False
        else:
            return {"has_account": False}


@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_vo_id=None):
    if request.method == "GET":
        attendee = Attendee.objects.filter(conference=conference_vo_id)
        return JsonResponse(
            {"attendees": attendee},
            encoder=AttendeeListEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        print(ConferenceVO)
        try:
            conference_href = f"/api/conferences/{conference_vo_id}/"
            conference = ConferenceVO.objects.get(import_href=conference_href)
            content["conference"] = conference
        except ConferenceVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )
        attendee = Attendee.objects.create(**content)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )


# class ConferenceVODetailEncoder(ModelEncoder):
#     model = ConferenceVO
#     properties = [
#         "name",
#         "import_href",
#     ]


# class AttendeeListEncoder(ModelEncoder):
#     model = Attendee
#     properties = [
#         "name",
#     ]

# def get_extra_data(self, o):
#     return {"Conference": o.conference}


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_attendee(request, id):
    if request.method == "GET":
        print("hi")
        attendee = Attendee.objects.get(id=id)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Attendee.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "conference" in content:
                conference = ConferenceVO.objects.get(id=content["conference"])
                content["conference"] = conference
        except ConferenceVO.DoesNotExist:
            return JsonResponse(
                {"conference": "conference does not exist"}, status=400
            )
        Attendee.objects.filter(id=id).update(**content)
        attendee = Attendee.objects.get(id=id)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )


# class AttendeeDetailEncoder(ModelEncoder):
#     model = Attendee
#     properties = [
#         "email",
#         "name",
#         "company_name",
#         "created",
#         "conference",
#     ]

#     encoders = {
#         "conference": ConferenceVODetailEncoder(),
#     }

#     def get_extra_data(self, o):
#         # Get the count of AccountVO objects with email equal to o.email
#         count = AccountVO.objects.filter(email=o.email).count()
#         # test = AccountVO.objects.get(email=o.email)
#         # print(test)
#         print(count)
#         print(o.email)
#         # Return a dictionary with "has_account": True if count > 0
#         if count > 0:
#             return {"has_account": True}
#         # Otherwise, return a dictionary with "has_account": False
#         else:
#             return {"has_account": False}

# def get_extra_data(self, o):
#     count = 0
#     if AccountVO.objects.get(email=o.email):
#         count += 1
#     return {"has account": count > 0}


# @require_http_methods(["DELETE", "GET", "PUT"])
# def api_show_conference(request, id):
#     if request.method == "GET":
#         conference = Conference.objects.get(id=id)
#         return JsonResponse(
#             {"conference": conference},
#             encoder=ConferenceDetailEncoder,
#             safe=False,
#         )
#     elif request.method == "DELETE":
#         count, _ = Conference.objects.filter(id=id).delete()
#         return JsonResponse({"deleted": count > 0})
#     else:
#         content = json.loads(request.body)
#         try:
#             if "location" in content:
#                 location = Location.objects.get(content["location"])
#                 content["location"] = location
#         except Location.DoesNotExist:
#             return JsonResponse(
#                 {"message": "Invalid Location"},
#                 status=400,
#             )
#         conference = Conference.objects.filter(id=id).update(**content)
#         return JsonResponse(
#             {"conference": conference},
#             encoder=ConferenceDetailEncoder,
#             safe=False,
#         )
